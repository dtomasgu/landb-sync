# landb-sync

[![Build Status](https://gitlab.cern.ch/kubernetes/networking/badges/master/pipeline.svg)](https://gitlab.cern.ch/kubernetes/networking/pipelines)
[![Go Report Card](https://goreportcard.com/badge/gitlab.cern.ch/kubernetes/networking)](https://goreportcard.com/report/gitlab.cern.ch/kubernetes/networking)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

landb-sync automatically validates and registers hostnames from Kubernetes
Ingress resources into the CERN network database (landb).

## Requirements

* Kubernetes >= v1.21

## Application

The landb-sync follows a command line as:

```
landb-sync <command> [<args>]
```

The commands available are:

* serve - the main application that manages the kubernetes ingress creation/update/delete as well as the number of ingress nodes.
* dnssolver - the CERN application that allows to configure Let's Encrypt certificates using the DNS Challenge


## Overview - Main App

There are three main components:

* A [ValidatingAdmissionWebhook](https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/) managing ingress creation/update requests. It is responsible for checking if the respective hostname is available for use, allowing (or not) the creation of the ingress object.

* An ingress watcher that subscribes to the ingress event feed and processes create/update/delete events accordingly interacting with the Openstack nodes to automatically add, update or remove the evaluated hostname from the node landb-alias.

* A periodic (__state-check-period__) process that:

    * Insures that __num-ingress-nodes__ are available, adding and removing nodes to meet the desired amount.
    * Check and update ingress node alias against defined kubernetes ingress resources.

### Available serve command arguments

* __num-ingress-nodes__: The number of desired ingress nodes (default: 2).
* __state-check-period__: The period between DNS state checks and updates (default: 5m).
* __cloud-config__: Path to the OpenStack cloud configuration file (default: /config).
* __tls-cert-file__: the path to the certificate to be used by the validation server (default: /etc/webhook/certs/server.crt).
* __tls-key-file__: the path to the keyfile to be used by the validation server (default: /etc/webhook/certs/server.key).
* __bypass-cern-dns__: Allow registration of ingress hostnames if they are attributed to IPs on landb but they belong to this cluster ingress nodes (default: true).
* __landb-alias-enabled__: enable/disable landb-sync management of the landb-alias property (default: true).
* __landb-set-enabled__: enable/disable landb-sync management of the landb-set property (default: false).
* __landb-set-name__: landb set name to be used for the ingress nodes (default: "").

## Overview - DNS solver

The DNS solver application interacts with CERN's DNS server using REST API to expose DNS Challenge Tokens
to outside CERN without needing to open a firewall. Thus allowing applications to get Let's Encrypt certificates.

To enabled this you first need to install cert-manager. Using cluster label is advised.
If you installed cert-manager manually, you need to configure:
* certManager.serviceAccountName: <cert-manager Service Account>
* certManager.namespace: <cert-manager namespace>

1. Install this chart:
```bash
helm repo add cern https://registry.cern.ch/chartrepo/cern
helm repo upadte cern
helm upgrade -i landb-sync cern/magnum
```

2. Create Issuer/Cluster Issuer with the proper DNS challenge config:
```yaml
cat <<EOF > cluster-issuer-staging.yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: cern-issuer-staging
spec:
  acme:
    email: dy090.guerra@gmail.com
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: cern-cluster-issuer-key
    solvers:
    - dns01:
        cnameStrategy: Follow
        webhook:
          groupName: "acme.kubernetes.cern.ch"
          solverName: "cern"
          config:
            tokenSecretRef:
              key: auth
              name: mysecret
      selector:
        dnsZones:
        - 'cern.ch'
---
apiVersion: v1
data:
  auth: <base64 encoded USER:PASS> 
kind: Secret
metadata:
  name: mysecret
  namespace: kube-system
type: Opaque
EOF
kubectl apply -f cluster-issuer-staging.yaml
```

3. Deploy your application with an ingress requesting a signed certificate
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-loadbalance
  name: nginx-deployment
spec:
  replicas: 5
  selector:
    matchLabels:
      app: nginx-loadbalance
  template:
    metadata:
      labels:
        app: nginx-loadbalance
    spec:
      containers:
      - image: nginx:1.15.5
        imagePullPolicy: Always
        name: nginx
        ports:
        - containerPort: 80
          protocol: TCP
        resources: {}
---
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
spec:
  ports:
  - name: http
    port: 8080
    protocol: TCP
    targetPort: 80
  - name: https
    port: 8443
    protocol: TCP
    targetPort: 443
  selector:
    app: nginx-loadbalance
  type: ClusterIP
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    cert-manager.io/cluster-issuer: cern-issuer-staging
    kubernetes.io/ingress.class: traefik
    traefik.frontend.entryPoints: https
    traefik.ingress.kubernetes.io/router.entrypoints: https
    traefik.ingress.kubernetes.io/router.tls: "true"
  name: nginx-ingress
spec:
  rules:
  - host: my-demo-app.cern.ch
    http:
      paths:
      - backend:
          service:
            name: nginx-service
            port:
              number: 8080
        path: /
        pathType: Prefix
  tls:
  - hosts:
    - my-demo-app.cern.ch
    secretName: my-app-tls-ingress-key
```

4. Wait for the DNS to syncronize