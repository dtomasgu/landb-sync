// Copyright CERN.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package main

import (
	"flag"
	"os"

	"github.com/cert-manager/cert-manager/cmd/util"
	"github.com/cert-manager/cert-manager/pkg/acme/webhook/cmd/server"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.cern.ch/kubernetes/networking/landb-sync/pkg/dns"
)

var (
	options *server.WebhookServerOptions
)

// Based on https://github.com/cert-manager/cert-manager/blob/master/pkg/acme/webhook/cmd/server/start.go
var dnssolverCmd = &cobra.Command{
	Use:   "dnssolver",
	Short: "Launches the DNS-01 ACME Let's Encrypt CERN solver API server",
	Long: `
Launches the Let's Encrypt DNS-01 solver server that handles certificate
signing requests and validates them by manipulating the DNS name TXT record
available on the CERN's DNS server.`,
	Run: func(cmd *cobra.Command, args []string) {

		log.SetFormatter(&log.JSONFormatter{})
		lvl, err := log.ParseLevel(log.DebugLevel.String())
		if err != nil {
			log.Warnf("Unrecognised logLevel %s. Error:  %s", logLevel, err)
			lvl = log.InfoLevel
		}
		log.Infof("Setting log level to %s.", lvl)
		log.SetLevel(lvl)

		stopCh, exit := util.SetupExitHandler(util.GracefulShutdown)
		defer exit() // This function might call os.Exit, so defer last

		if err := options.RunWebhookServer(stopCh); err != nil {
			return
		}
	},
}

func init() {
	options = server.NewWebhookServerOptions(os.Stdout, os.Stderr, "acme.kubernetes.cern.ch", &dns.CernDNSProviderSolver{})

	flags := dnssolverCmd.Flags()
	options.RecommendedOptions.AddFlags(flags)

	dnssolverCmd.Flags().AddGoFlagSet(flag.CommandLine)
	rootCmd.AddCommand(dnssolverCmd)
}
