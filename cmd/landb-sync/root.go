// Copyright CERN.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.cern.ch/kubernetes/networking/landb-sync/pkg/version"
)

var (
	rootCmd = &cobra.Command{
		Use:   "landb-sync",
		Short: "Manages updates between cluster resources and network db",
		Long: `
Landb-sync is used to manage updates between the cern cluster
and the network resources at CERN.
`,
		Version: fmt.Sprintf("%v %.7v %v", version.Version(), version.Commit(),
			version.Metadata()),
		Hidden: true,
	}
)

func init() {
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func main() {
	rootCmd.SetVersionTemplate(
		`{{with .Name}}{{printf "%s " .}}{{end}}{{printf "%s" .Version}}
`)
	Execute()
}
