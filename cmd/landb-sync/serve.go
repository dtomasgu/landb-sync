// Copyright CERN.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/robfig/cron"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.cern.ch/kubernetes/networking/landb-sync/pkg/landb_sync"
)

var (
	port              int
	certFile          string
	keyFile           string
	numIngressNodes   int
	duration          time.Duration
	cloudConfig       string
	bypassDNS         bool
	landbAliasEnabled bool
	landbSetEnabled   bool
	landbSetName      string
	logLevel          string
)

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Main landb-sync utility to manage landb-alias and landb-sets",
	Long: `
Launches the utility to syncronize defined Kubernetes
Ingress hostnames and also, if enabled, the configuration of the
landbSet on this nodes. At the same time it makes sure that the
defined number of ingress nodes are currently available.`,
	Run: func(cmd *cobra.Command, args []string) {

		log.SetFormatter(&log.JSONFormatter{})
		lvl, err := log.ParseLevel(logLevel)
		if err != nil {
			log.Warnf("Unrecognised logLevel %s. Error:  %s", logLevel, err)
			lvl = log.InfoLevel
		}
		log.Infof("Setting log level to %s.", lvl)
		log.SetLevel(lvl)

		// Ingress Nodes must be Natural or Zero
		if numIngressNodes < 0 {
			numIngressNodes = 0
		}

		landbSync := landb_sync.LandbSync{
			NumIngressNodes:     numIngressNodes,
			CloudConfig:         cloudConfig,
			ServerBypassCernDNS: bypassDNS,
			StateCheckPeriod:    duration,
			LandbAliasEnabled:   landbAliasEnabled,
			LandbSetEnabled:     landbSetEnabled,
			LandbSetName:        landbSetName,
		}

		var whsvr *landb_sync.WebhookServer
		if landbAliasEnabled {
			pair, err := tls.LoadX509KeyPair(certFile, keyFile)
			if err != nil {
				log.Fatalf("Failed to load key pair: %v", err)
			}

			whsvr = &landb_sync.WebhookServer{
				Server: &http.Server{
					Addr:      fmt.Sprintf(":%v", port),
					TLSConfig: &tls.Config{Certificates: []tls.Certificate{pair}},
				},
				LandbSync: landbSync,
			}
		}

		// Init k8s and OS conn Handlers
		landbSync.Init()
		// Populate the initial state before launching server
		err = landbSync.UpdateStateMachine()
		if err != nil {
			log.Fatalf("Failed to init StateMachine: %v", err)
		}

		// define http server and server handler
		mux := http.NewServeMux()
		if landbAliasEnabled {
			mux.HandleFunc("/validate", whsvr.Serve)
			whsvr.Server.Handler = mux

			// start webhook server in new routine
			go func() {
				if err := whsvr.Server.ListenAndServeTLS("", ""); err != nil {
					log.Fatalf("Failed to launch ListenAndServer webhook server: %v", err)
				}
			}()
			log.Info("Webhook server started")
		} else {
			log.Info("Webhook server was not started as landbAliasEnabled flag is false")
		}

		// start cron scheduler in new routine
		c := cron.New()
		err = c.AddFunc(fmt.Sprintf("@every %v", landbSync.StateCheckPeriod), func() {
			err := landbSync.UpdateStateMachine()
			if err != nil {
				log.Fatalf("Failed to update StateMachine: %v", err)
			}
		})
		if err != nil {
			log.Fatalf("Could not add UpdateStateMachine cronloop: %v", err)
		}
		c.Start()
		log.Info("Sucessfully scheduled UpdateStateMachine periodic updates")

		// Register Ingresses watcher
		stop := make(chan int)
		if landbAliasEnabled {
			go landbSync.RegisterIngressWatcher(stop)
			log.Info("Sucessfully launched the k8s ingress watcher")
		} else {
			log.Info("Ingress watcher was not launched as landbAliasEnabled flag is false")
		}

		// listening OS shutdown signal
		signalChan := make(chan os.Signal, 1)
		signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)
		<-signalChan

		log.Info("Got OS shutdown signal, shutting down systems...")
		log.Info("Stoping Ingress Watch")
		close(stop)
		log.Info("Stop cron scheduler")
		c.Stop() // Stop the scheduler (does not stop any jobs already running).
		if landbAliasEnabled {
			log.Info("Stop webhook server")
			err = whsvr.Server.Shutdown(context.Background())
			if err != nil {
				log.Errorf("Failed to shutdown server: %v\n", err)
			}
		}
	},
}

func init() {
	serveCmd.Flags().IntVarP(&port, "port", "p", 443, "webhook server port")
	serveCmd.Flags().StringVarP(&certFile, "tls-cert-file", "t", "/etc/webhook/certs/cert.pem", "x509 certificate file for https")
	serveCmd.Flags().StringVarP(&keyFile, "tls-key-file", "k", "/etc/webhook/certs/key.pem", "x509 key file for https")
	serveCmd.Flags().IntVarP(&numIngressNodes, "num-ingress-nodes", "n", 2, "number of nodes to act as ingress in the cluster")
	serveCmd.Flags().DurationVarP(&duration, "state-check-period", "d", time.Minute*5, "check interval for ingress and node ingress state")
	serveCmd.Flags().StringVarP(&cloudConfig, "cloud-config", "c", "/config", "path to the openstack config file")
	serveCmd.Flags().BoolVar(&bypassDNS, "bypass-cern-dns", true, "")
	serveCmd.Flags().BoolVar(&landbAliasEnabled, "landb-alias-enabled", true, "enable/disable landb-sync management of the landb-alias property")
	serveCmd.Flags().BoolVar(&landbSetEnabled, "landb-set-enabled", false, "enable/disable landb-sync management of the landb-set property")
	serveCmd.Flags().StringVarP(&landbSetName, "landb-set-name", "s", "", "landb set name to be used for the ingress nodes")
	serveCmd.Flags().StringVarP(&logLevel, "log-level", "v", "", "set the debug level to use {Trace,Debug,Info,Warn,Error,Fatal,Panic}")
	rootCmd.AddCommand(serveCmd)
}
