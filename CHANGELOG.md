# Changelog

---

## v0.3.0-pre (xx/11/2022)

#### New:

- [dnsSolver](https://gitlab.cern.ch/kubernetes/networking/landb-sync/-/issues/5) Add DNS-01 acme let's encrypt cert-manager solver for CERN's network.

#### Improvements:

- [landb-sync](https://gitlab.cern.ch/kubernetes/automation/charts/cern) Move helm chart into project as bin and chart depend on each other.

---

## v0.2.1 (29/01/2020)

#### Bug Fixes:

- [OS-10675](https://its.cern.ch/jira/browse/OS-10675) Periodic update routine was not being called.

---
