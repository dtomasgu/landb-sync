// Copyright CERN.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package landb_sync

import (
	"math/rand"
	"time"

	"github.com/pkg/errors"
	v1Type "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// RemoveDuplicatesFromSlice Deduplicates strings on a slice
func RemoveDuplicatesFromSlice(s []string) []string {
	m := make(map[string]bool)
	for _, item := range s {
		m[item] = true
	}

	var result []string
	for item := range m {
		result = append(result, item)
	}
	return result
}

// ShuffleSlice Shuffles the elements of the slice len(slice) number of times
func ShuffleSlice(sliceP []v1Type.Node) {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(sliceP), func(i, j int) {
		sliceP[i], sliceP[j] = sliceP[j], sliceP[i]
	})
}

// Contains will find if substring exists in []string
func Contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// NodesL is a slice of Nodes
type NodesL []v1Type.Node

func (s NodesL) Len() int {
	return len(s)
}
func (s NodesL) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s NodesL) Less(i, j int) bool {
	for pos := 0; pos < len(s[i].Name); pos++ {
		if s[i].Name[pos] < s[j].Name[pos] {
			return true
		}
		if s[i].Name[pos] > s[j].Name[pos] {
			return false
		}
	}
	return false
}

// DropStringFromSlice drops string (s) from a string slice (ss)
func DropStringFromSlice(s string, ss []string) []string {
	for i, v := range ss {
		if v == s {
			ss = append(ss[:i], ss[i+1:]...)
			break
		}
	}
	return ss
}

// GetKubernetesClientset get kubernetes configuration and generates client handle
func GetKubernetesClientset() (*kubernetes.Clientset, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, errors.Wrap(err, "could not read in-cluster config")
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, errors.Wrap(err, "could not create clientset")
	}

	return clientset, nil
}
