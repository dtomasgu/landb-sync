// Copyright CERN.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package landb_sync

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"sync"
	"time"

	"gopkg.in/gcfg.v1"

	log "github.com/sirupsen/logrus"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	netutil "k8s.io/apimachinery/pkg/util/net"
	certutil "k8s.io/client-go/util/cert"

	"gitlab.cern.ch/kubernetes/networking/landb-sync/pkg/version"
)

// LandbSync stores the OS and Kubernetes conn handles and options
type LandbSync struct {
	// LandbAliasEnabled defines if landb-sync should manage registration of alias
	LandbAliasEnabled bool
	// LandbSetEnabled defines if landb-sync should manage registration of ingress nodes into the landb-set
	LandbSetEnabled bool
	// LandbSetName defines the name to be used for the landb-set when managing this property
	LandbSetName string
	// NumIngressNodes is the number of ingress instances to be kept active
	NumIngressNodes int
	// Allow ingress to be created if the existent landb-alias references self
	ServerBypassCernDNS bool
	// StateCheckPeriod is the interval between syncs
	StateCheckPeriod time.Duration
	// CloudConfig is the location of the openstack cloud config
	CloudConfig  string
	config       *rest.Config
	clientset    *kubernetes.Clientset
	osProvider   *gophercloud.ProviderClient
	serverClient *gophercloud.ServiceClient
}

// Init sets all the required clients to talk to openstack
func (d *LandbSync) Init() {
	// init Lock
	cluster.mutex = &sync.Mutex{}

	// ////////////////////////////////// K8s Config
	// get K8s config
	var err error
	d.config, err = rest.InClusterConfig()
	if err != nil {
		log.Fatalf("Could not get Kubernetes configuration options: %v", err)
	}
	// creates the clientset
	d.clientset, err = kubernetes.NewForConfig(d.config)
	if err != nil {
		log.Fatalf("Could not create the client set to comunicate with the Kubernetes engine: %v", err)
	}

	// ////////////////////////////////// OS Config
	// HACK: This package (Config) comes from providerOpenStack.go to fix
	// 				the apimachinery dependencies headache
	var cfg Config
	if err := gcfg.ReadFileInto(&cfg, d.CloudConfig); err != nil {
		log.Fatalf("Could not get OpenStack configuration file: %v", err)
	}

	// HACK: This package (toAuthOptsExt) comes from providerOpenStack.go to fix
	// 				the apimachinery dependencies headache
	authOpts := toAuthOptsExt(cfg)

	d.osProvider, err = openstack.NewClient(cfg.Global.AuthURL)
	if err != nil {
		log.Fatalf("Could not authenticate client: %v", err)
	}

	if cfg.Global.CAFile != "" {
		roots, err := certutil.NewPool(cfg.Global.CAFile)
		if err != nil {
			log.Fatalf("Could not create new Certificate Pool: %v", err)
		}
		config := &tls.Config{}
		config.RootCAs = roots
		d.osProvider.HTTPClient.Transport = netutil.SetOldTransportDefaults(&http.Transport{TLSClientConfig: config})

	}

	userAgent := gophercloud.UserAgent{}
	userAgent.Prepend(fmt.Sprintf("landb-sync/%s", version.Version()))
	log.Infof("user-agent: %s", userAgent.Join())
	d.osProvider.UserAgent = userAgent

	err = openstack.AuthenticateV3(d.osProvider, authOpts, gophercloud.EndpointOpts{})
	if err != nil {
		log.Fatalf("Could not authenticate: %v", err)
	}

	d.serverClient, err = openstack.NewComputeV2(d.osProvider, gophercloud.EndpointOpts{Region: cfg.Global.Region})
	if err != nil {
		log.Fatalf("Could not get client for the to interact with Server API: %v", err)
	}
}

// UpdateStateMachine refreshes the state machine
func (d *LandbSync) UpdateStateMachine() (err error) {
	cluster.mutex.Lock()
	defer cluster.mutex.Unlock()

	// Clear previous data
	cluster.oldIngresses = cluster.ingresses
	cluster.roleIngressNodes = cluster.roleIngressNodes[:0]
	cluster.notRoleIngressNodes = cluster.notRoleIngressNodes[:0]

	// Check and Update Node role=ingress
	err = cluster.GetKubernetesNodesIngressStatus(d)
	if err != nil {
		return fmt.Errorf("could not get role=ingress nodes: %v", err)
	}
	err = d.UpdateIngressNodes(&cluster)
	if err != nil {
		return fmt.Errorf("could not update kubernetes role=ingress nodes: %v", err)
	}

	if d.LandbSetEnabled {
		go func() {
			err = d.UpdateNodesLandbSet(&cluster, false)
			if err != nil {
				log.Errorf("Error updating nodes landb-set property: %v", err)
			}
		}()
	}

	if d.LandbAliasEnabled {
		// Check and Update all Alias in Ingress Nodes using kubernetes ingresses
		err = cluster.GetKubernetesIngresses(d)
		if err != nil {
			return fmt.Errorf("could not update kubernetes ingress resources: %v", err)
		}
		// If Aliases have been added OR deleted, and if nodes have been added we need to update landb Inggresses
		err = d.UpdateIngressAlias(&cluster)
		if err != nil {
			return fmt.Errorf("could not update landb aliases: %v", err)
		}
	}

	log.Info("UpdateStateMachine: All done")
	return nil
}
