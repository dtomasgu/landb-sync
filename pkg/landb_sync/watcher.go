// Copyright CERN.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package landb_sync

import (
	"context"
	"fmt"
	"strings"

	// Logger
	log "github.com/sirupsen/logrus"
	// k8s object types
	networking "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	watch "k8s.io/apimachinery/pkg/watch"
)

// RegisterIngressWatcher will create the Ingress Watcher and map the functions
// the triggers of the Create/Update/Delete operations
func (d *LandbSync) RegisterIngressWatcher(stop <-chan int) {
	reopen := false
	resourceVersion := "0" //Track events so we don list already processed events
	log.Infof("Ingress watch Resource Version %s", resourceVersion)
	for {
		watcher, err := d.clientset.NetworkingV1().Ingresses("").Watch(context.TODO(), metav1.ListOptions{ResourceVersion: resourceVersion})
		if err != nil {
			log.Fatalf("Error creating watcher for Ingress objects: %v", err)
		}
		log.Info("Ingress watcher registered...")
		for {
			select {
			case <-stop:
				log.Info("Exiting RegisterIngressWatcher")
				return
			case event := <-watcher.ResultChan():
				ingress, ok := event.Object.(*networking.Ingress)
				if !ok {
					reopen = true
					log.Warning("Unexpected type on Ingress watcher")
				} else {
					resourceVersion = ingress.ResourceVersion
				}

				switch event.Type {
				case watch.Added:
					err := d.handleIngressAddedEvent(ingress)
					if err != nil {
						log.Errorf("Error on handleIngressAddedEvent: %v", err)
					}
				case watch.Modified:
					err := d.handleIngressModifiedEvent(ingress)
					if err != nil {
						log.Errorf("Error on handleIngressModifiedEvent: %v", err)
					}
				case watch.Deleted:
					err := d.handleIngressDeletedEvent(ingress)
					if err != nil {
						log.Errorf("Error on handleIngressDeletedEvent: %v", err)
					}
				}
			}
			if reopen {
				reopen = false
				break
			}
		}
	}
}

func (d *LandbSync) handleIngressDeletedEvent(ingEvent *networking.Ingress) (err error) {
	log.Infof("New delete event for ingress %s", ingEvent.Name)

	cluster.mutex.Lock()
	defer cluster.mutex.Unlock()

	// Drop the ingress from the current state of the cluster and trigger alias update
	for i, ing := range cluster.ingresses {
		if ing.Name == ingEvent.Name && ing.Namespace == ingEvent.Namespace {
			cluster.ingresses[len(cluster.ingresses)-1], cluster.ingresses[i] = cluster.ingresses[i], cluster.ingresses[len(cluster.ingresses)-1]
			cluster.ingresses = cluster.ingresses[:len(cluster.ingresses)-1]
			break
		}
	}
	var eventList []networking.Ingress
	eventList = append(eventList, *ingEvent)
	kubernetesDeletedAliases := ExtractHostnamesFromKubernetesIngresses(eventList)
	err = d.UpdateIngressAliasFromList(&cluster, ExtractHostnamesFromKubernetesIngresses(cluster.ingresses), kubernetesDeletedAliases)
	if err != nil {
		return fmt.Errorf("error handling ingress delete event: %v", err)
	}
	return nil
}

func (d *LandbSync) handleIngressAddedEvent(ingEvent *networking.Ingress) (err error) {
	log.Infof("New Add event for ingress %s", ingEvent.Name)
	newAliases := cluster.aliases

	cluster.mutex.Lock()
	defer cluster.mutex.Unlock()
	// Add the ingress to the current state of the cluster and trigger update
	cluster.ingresses = append(cluster.ingresses, *ingEvent)
	for i, rule := range ingEvent.Spec.Rules {
		// Only run for ingresses where hostnames are defined
		if len(rule.Host) == 0 {
			log.Infof("Ingress %s rule %d is not a valid candidate to be added as alias", ingEvent.Name, i+1)
			continue
		}
		hostname := strings.TrimSuffix(rule.Host, ".cern.ch")
		if Contains(newAliases, hostname) {
			log.Infof("Ingress %s rule %d has no modifications on hostname. Skipping...", ingEvent.Name, i+1)
			continue
		} else {
			newAliases = append(newAliases, hostname)
		}
	}

	err = d.UpdateIngressAliasFromList(&cluster, newAliases, []string{})
	if err != nil {
		return fmt.Errorf("error handling ingress add event: %v", err)
	}
	return nil
}

func (d *LandbSync) handleIngressModifiedEvent(ingEvent *networking.Ingress) (err error) {
	log.Infof("New Update event for ingress %s", ingEvent.Name)
	newAliases := cluster.aliases

	cluster.mutex.Lock()
	defer cluster.mutex.Unlock()

	for i, rule := range ingEvent.Spec.Rules {
		// Only run for ingresses where hostnames are defined
		if len(rule.Host) == 0 {
			log.Infof("Ingress %s rule %d is not a valid candidate to be modified as alias", ingEvent.Name, i+1)
			continue
		}
		hostname := strings.TrimSuffix(rule.Host, ".cern.ch")
		if Contains(newAliases, hostname) {
			log.Infof("Ingress %s rule %d has no modifications on hostname. Skipping...", ingEvent.Name, i+1)
			continue
		} else {
			err = cluster.GetKubernetesIngresses(d)
			if err != nil {
				log.Warningf("Could not update defined aliases from the cluster: %v", err)
			}
			newAliases = ExtractHostnamesFromKubernetesIngresses(cluster.ingresses)
			break
		}
	}

	for _, oldIngress := range cluster.oldIngresses {
		if oldIngress.UID == ingEvent.UID {
			var kubernetesDeletedAliases []string
			for _, al := range newAliases {
				kubernetesDeletedAliases = DropStringFromSlice(al, cluster.aliases)
			}
			err = d.UpdateIngressAliasFromList(&cluster, newAliases, kubernetesDeletedAliases)
			if err != nil {
				return fmt.Errorf("error handling ingress update event: %v", err)
			}
			return nil
		}
	}
	return nil
}
