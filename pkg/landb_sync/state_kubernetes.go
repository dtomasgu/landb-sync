// Copyright CERN.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package landb_sync

import (
	"context"
	"encoding/json"
	"fmt"
	"sort"
	"strings"
	"sync"

	// Logger
	log "github.com/sirupsen/logrus"
	//// Kubernetes
	v1Type "k8s.io/api/core/v1"
	networking "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

//  patchStringValue specifies a patch operation.
type patchStringValue struct {
	Op    string `json:"op"`
	Path  string `json:"path"`
	Value string `json:"value"`
}

type stateData struct {
	// Lock
	mutex *sync.Mutex
	// Kubernetes
	roleIngressNodes    []v1Type.Node
	notRoleIngressNodes []v1Type.Node
	ingresses           []networking.Ingress
	oldIngresses        []networking.Ingress

	// Defined aliases (only hostname)
	aliases []string
}

var cluster stateData

// GetKubernetesNodesIngressStatus will request all nodes from the Kubernetes cluster
// split by role=ingress label and update the kubernetes object accordingly
func (kc *stateData) GetKubernetesNodesIngressStatus(landbSync *LandbSync) (err error) {
	// 1. Get all Nodes
	nodesList, err := landbSync.clientset.CoreV1().Nodes().List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return fmt.Errorf("error obtaining kubernetes nodes list: %v", err)
	}
	nodes := nodesList.Items
	log.Infof("There are %d node(s) associated to the cluster", len(nodes))
	// 2. Split nodes by role=ingress label
	for _, node := range nodes {
		if value, ok := node.Labels["role"]; ok && value == "ingress" {
			kc.roleIngressNodes = append(kc.roleIngressNodes, node)
		} else {
			kc.notRoleIngressNodes = append(kc.notRoleIngressNodes, node)
		}
	}
	return nil
}

// FilterMasterNodes will filter out all the master nodes from the slice
func FilterMasterNodes(nodesList []v1Type.Node) (filtered []v1Type.Node) {
	for _, node := range nodesList {
		if _, found := node.ObjectMeta.Labels["node-role.kubernetes.io/master"]; !found {
			filtered = append(filtered, node)
		}
	}
	return filtered
}

// FilterNodesByConditionTypeValue will filter the specified []Node by the
// defined key:value Node.Condition. It will return a []Node with the
// corresponding Nodes filtered in this way
func FilterNodesByConditionTypeValue(nodesList []v1Type.Node, key string, values []string) (filtered []v1Type.Node) {
	for _, node := range nodesList {
		for _, cond := range node.Status.Conditions {
			if string(cond.Type) != key {
				continue
			}
			for _, value := range values {
				if string(cond.Status) == value {
					filtered = append(filtered, node)
					break
				}
			}
		}
	}
	return filtered
}

// UpdateNodeNotIngressToIngress will move the node with nodeName from notIngress to ingress state
func (kc *stateData) UpdateNodeNotIngressToIngress(nodeName string) {
	// Search for the Node we want to replace
	for i, node := range kc.notRoleIngressNodes {
		if node.Name == nodeName {
			// Add node to dst
			kc.roleIngressNodes = append(kc.roleIngressNodes, kc.notRoleIngressNodes[i])
			// Remove node from src
			kc.notRoleIngressNodes[i] = kc.notRoleIngressNodes[len(kc.notRoleIngressNodes)-1]
			kc.notRoleIngressNodes = kc.notRoleIngressNodes[:len(kc.notRoleIngressNodes)-1]
			return
		}
	}
}

// UpdateNodeIngressToNotIngress will move the node with nodeName from ingress to notIngress state
func (kc *stateData) UpdateNodeIngressToNotIngress(nodeName string) {
	// Search for the Node we want to replace
	for i, node := range kc.roleIngressNodes {
		if node.Name == nodeName {
			// Add node to dst
			kc.notRoleIngressNodes = append(kc.notRoleIngressNodes, kc.roleIngressNodes[i])
			// Remove node from src
			kc.roleIngressNodes[i] = kc.roleIngressNodes[len(kc.roleIngressNodes)-1]
			kc.roleIngressNodes = kc.roleIngressNodes[:len(kc.roleIngressNodes)-1]
			return
		}
	}
}

// AddLabelToNode will add the specified key:value to the Node.metadata.Labels
func (d *LandbSync) AddLabelToNode(node v1Type.Node, key string, value string) (err error) {
	payload := []patchStringValue{{
		Op:    "add",
		Path:  "/metadata/labels/" + key,
		Value: value,
	}}
	payloadBytes, _ := json.Marshal(payload)

	log.Infof("Adding label %s:%s to node: %s", key, value, node.Name)
	_, err = d.clientset.CoreV1().Nodes().Patch(context.TODO(), node.Name, types.JSONPatchType, payloadBytes, metav1.PatchOptions{FieldManager: "landb_sync"})
	if err != nil {
		return fmt.Errorf("error patching Node %s: %v", node.Name, err)
	}
	return nil
}

// DeleteLabelFromNode will delete the specified key from Node.metadata.Labels
func (d *LandbSync) DeleteLabelFromNode(node v1Type.Node, key string) (err error) {
	// Needs to update current state???
	payload := []patchStringValue{{
		Op:   "remove",
		Path: "/metadata/labels/" + key,
	}}
	payloadBytes, _ := json.Marshal(payload)

	log.Infof("Removing label %s from node: %s", key, node.Name)
	_, err = d.clientset.CoreV1().Nodes().Patch(context.TODO(), node.Name, types.JSONPatchType, payloadBytes, metav1.PatchOptions{FieldManager: "landb_sync"})
	if err != nil {
		return fmt.Errorf("error patching Node %s: %v", node.Name, err)
	}
	return nil
}

// DismissIngressNode will delete label role=ingress and landb-alias property from node
func (d *LandbSync) DismissIngressNode(node v1Type.Node) (err error) {
	// Before deleting role=ingress must deregister the landb-alias from OSnode
	err = d.DeleteOpenstackPropertyByNodeUUID("landb-alias", node.Status.NodeInfo.SystemUUID)
	if err != nil {
		log.Warningf("Error deleting metadata.landb-alias from node %s: %v", node.Name, err)
		return
	}
	// NOTE: Node can only be removed after property dropped, because if property
	// is not removed and node gets out of the role=ingress pool, landb-property
	// will never be scheduled to be deleted again. If this property stays in play
	// future OS commits that use the same --load-# will fail as this is already
	// attributed
	// Now we remove the label role=ingress from said node
	err = d.DeleteLabelFromNode(node, "role")
	if err != nil {
		log.Warningf("Error deleting role=ingress label from node %s: %v", node.Name, err)
	}
	return
}

// DropUnhealthyNodesFromIngressRole will remove all not healthy nodes from role ingress
func (d *LandbSync) DropUnhealthyNodesFromIngressRole(cluster *stateData) {
	// If Nodes NotReady && Ingress, drop node as ingress
	deprecateIngress := FilterNodesByConditionTypeValue(cluster.roleIngressNodes, "Ready", []string{"False", "Unknown"})
	for _, node := range deprecateIngress {
		log.Infof("Droping node %s from role=ingress as it is unhealthy.", node.Name)
		// Remove landb-alias and role=ingress from node
		err := d.DismissIngressNode(node)
		if err == nil {
			cluster.UpdateNodeIngressToNotIngress(node.Name)
		}
	}
}

// UpdateIngressNodes will add or remove Ingress Nodes to meet the desired capacity
func (d *LandbSync) UpdateIngressNodes(cluster *stateData) (err error) {
	var selectedNode v1Type.Node

	// Check and Update Node role=ingress
	nodeIngressList := FilterNodesByConditionTypeValue(cluster.roleIngressNodes, "Ready", []string{"True"})
	readyNodeList := FilterNodesByConditionTypeValue(cluster.notRoleIngressNodes, "Ready", []string{"True"})
	// Do not set master nodes as role=ingress
	readyNodeList = FilterMasterNodes(readyNodeList)

	// 0. If Node not healthy, drop node has ingress so it can be replaced
	d.DropUnhealthyNodesFromIngressRole(cluster)

	// 1. Count the Ready nodes labeled with role=ingress
	deltaNodes := d.NumIngressNodes - len(nodeIngressList)
	// 2.1. If CurrentIngressNodes < DesiredIngressNodes, add label to deltaNodes
	if deltaNodes > 0 {
		// Shufle nodes
		ShuffleSlice(readyNodeList)
		log.Infof("Adding nodes to the ingress pool: %d -> %d", len(nodeIngressList), d.NumIngressNodes)
		// path to add nodes
		for i := 0; i < deltaNodes; i++ {
			// if there are no nodes to add, we can exit
			if len(readyNodeList) == 0 {
				log.Warningf("No more nodes to add as ingress. %d of %d desired nodes are set as ingress.", len(cluster.roleIngressNodes), d.NumIngressNodes)
				break
			}
			// Pop First Node (selectedNode) from slice
			selectedNode, readyNodeList = readyNodeList[0], readyNodeList[1:]
			err = d.AddLabelToNode(selectedNode, "role", "ingress")
			if err == nil {
				cluster.UpdateNodeNotIngressToIngress(selectedNode.Name)
			} else {
				deltaNodes--
			}
		}
	}

	// 2.2. If CurrentIngressNodes > DesiredIngressNodes, remove label from deltaNodes
	if deltaNodes < 0 {
		// Shufle nodes
		ShuffleSlice(nodeIngressList)
		log.Infof("Removing nodes from the ingress pool: %d -> %d", len(nodeIngressList), d.NumIngressNodes)
		for i := 0; i > deltaNodes; i-- {
			// if there are no nodes to delete, we can exit
			if len(nodeIngressList) == 0 {
				break
			}
			// Pop First Node (selectedNode) from slice
			selectedNode, nodeIngressList = nodeIngressList[0], nodeIngressList[1:]
			// Remove landb-alias and role=ingress from node
			err = d.DismissIngressNode(selectedNode)
			if err == nil {
				cluster.UpdateNodeIngressToNotIngress(selectedNode.Name)
			}
		}
	}

	//3 Sort Nodes By node.Name
	sort.Sort(NodesL(cluster.roleIngressNodes))
	return
}

// GetKubernetesIngresses will get all ingresses objects from the kubernetes cluster
func (kc *stateData) GetKubernetesIngresses(landbSync *LandbSync) (err error) {
	// 1. Get all ingress alias defined in the cluster
	ingressesList, err := landbSync.clientset.NetworkingV1().Ingresses("").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return fmt.Errorf("error contacting cluster when obtaining ingress list: %v", err)
	}
	kc.oldIngresses = kc.ingresses
	kc.ingresses = ingressesList.Items
	log.Infof("There are %d ingress definition(s) in the cluster", len(kc.ingresses))
	return nil
}

// ExtractHostnamesFromKubernetesIngresses will get a []string list with all the aliases defined on the cluster state
func (kc *stateData) ExtractHostnamesFromKubernetesIngresses() (aliases []string) {
	return ExtractHostnamesFromKubernetesIngresses(kc.ingresses)
}

// ExtractHostnamesFromKubernetesIngresses will get a []string list with all the aliases defined by the ingress input
func ExtractHostnamesFromKubernetesIngresses(ingresses []networking.Ingress) (aliases []string) {
	for _, ingress := range ingresses {
		for _, rule := range ingress.Spec.Rules {
			if len(rule.Host) != 0 { //Some rules might not have hostnames
				aliases = append(aliases, strings.TrimSuffix(rule.Host, ".cern.ch"))
			}
		}
	}
	return RemoveDuplicatesFromSlice(aliases)
}
