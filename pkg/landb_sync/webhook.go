// Copyright CERN.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package landb_sync

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strings"

	// Logger
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"k8s.io/client-go/kubernetes"

	// k8s json object Serialiser
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"

	// k8s object types
	admission "k8s.io/api/admission/v1"
	admissionregistrationv1 "k8s.io/api/admissionregistration/v1"
	v1 "k8s.io/api/core/v1"
	networking "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	runtimeScheme = runtime.NewScheme()
	codecs        = serializer.NewCodecFactory(runtimeScheme)
	deserializer  = codecs.UniversalDeserializer()

	// (https://github.com/kubernetes/kubernetes/issues/57982)
	// TODO: unused?
	// defaulter = runtime.ObjectDefaulter(runtimeScheme)
)

// WebhookServer is the server object where handles are registered into
type WebhookServer struct {
	Server    *http.Server
	LandbSync LandbSync
}

func init() {
	_ = admissionregistrationv1.AddToScheme(runtimeScheme)
	_ = networking.AddToScheme(runtimeScheme)
	// defaulting with webhooks:
	// https://github.com/kubernetes/kubernetes/issues/57982
	_ = v1.AddToScheme(runtimeScheme)
}

/////////////////////////////

// validate ingress
func (whsvr *WebhookServer) validate(ar *admission.AdmissionReview) *admission.AdmissionResponse {
	var result *metav1.Status
	newAliases := cluster.aliases

	req := ar.Request

	log.Infof("New Create/Update event for Ingress")
	log.Debugf("%+v", ar)

	allowed := true
	cluster.mutex.Lock()
	defer cluster.mutex.Unlock()
	switch req.Kind.Kind {
	case "Ingress":
		// if req.Operation == "CREATE" && req.Operation == "UPDATE"
		// Extract data
		var ingress networking.Ingress
		if err := json.Unmarshal(req.Object.Raw, &ingress); err != nil {
			msg := fmt.Sprintf("Could not unmarshal raw object: %v", err)
			log.Error(msg)
			result = &metav1.Status{Message: msg}
			allowed = false
			break //break from switch
		}
		// Validate and add alias to available role ingress hosts
		for i, rule := range ingress.Spec.Rules {
			// Ingresses can have many types. Our only interest is if they include hostname
			if len(rule.Host) == 0 {
				log.Infof("Ingress %s rule %d is not a valid candidate to be added as alias", req.Name, i+1)
				continue
			}
			hostname := strings.TrimSuffix(rule.Host, ".cern.ch")
			// If we are updating, hostname might be the same
			if req.Operation == admission.Update {
				// check if the hostname exists in the old object
				if Contains(newAliases, hostname) {
					log.Infof("Ingress %s rule %d has no modifications on hostname. Skipping...", req.Name, i+1)
					continue
				}
			}

			if addrs, ok := whsvr.authorizeIngressCreation(rule.Host); ok {
				// We are not able to add if we have no ingress nodes
				if len(cluster.roleIngressNodes) == 0 {
					log.Warning("There are no defined ingress nodes where to commit or remove landb properties")
					break
				}
			} else {
				msg := fmt.Sprintf("Endpoint %s is already registered to the following IPs: %v", hostname, addrs)
				log.Warning(msg)
				allowed = false
				result = &metav1.Status{Message: msg}
				break // break from for cycle
			}
		} // END of CREATE UPDATE
	}

	// Create response struct and return
	log.Infof("Valid ingress: %t", allowed)
	return &admission.AdmissionResponse{
		Allowed: allowed,
		Result:  result,
	}
}

// authorizeIngressCreation will validate if the hostname is accepted
func (whsvr *WebhookServer) authorizeIngressCreation(hostname string) ([]string, bool) {
	if whsvr.LandbSync.ServerBypassCernDNS {
		log.Info("Bypassing DNS hostname verfification as per configurations.")
		return []string{}, true
	}

	// Validate DNS is not attributed
	addrs, err := net.LookupHost(hostname)
	// If lookup finds no host it is an error, but in this case means hostname available
	if err != nil {
		log.Infof("The hostname %s is available and can be used.", hostname)
		return addrs, true
	}

	// If the hostname is in use, check if all the IPs belong to our cluster
	var clusterAddresses []string
	for _, node := range cluster.roleIngressNodes {
		for _, address := range node.Status.Addresses {
			clusterAddresses = append(clusterAddresses, address.Address)
		}
	}
	for _, addr := range addrs {
		// if the resolved IPs belong to our ingress nodes, allow Adding aliases
		if !Contains(clusterAddresses, addr) {
			log.Warningf("Landb-sync detected that the hostname is already atributed to out of cluster IP %s.", addr)
			return addrs, false
		}
	}
	log.Debug("All IP addresses obtained for the hostname are registered to this cluster.")
	return addrs, true
}

// Serve method for webhook server
func (whsvr *WebhookServer) Serve(w http.ResponseWriter, r *http.Request) {
	var body []byte
	if r.Body != nil {
		if data, err := ioutil.ReadAll(r.Body); err == nil {
			body = data
		}
	}
	if len(body) == 0 {
		log.Error("empty body")
		http.Error(w, "empty body", http.StatusBadRequest)
		return
	}

	// verify the content type is accurate
	contentType := r.Header.Get("Content-Type")
	if contentType != "application/json" {
		log.Errorf("Content-Type=%s, expect application/json", contentType)
		http.Error(w, "invalid Content-Type, expect `application/json`", http.StatusUnsupportedMediaType)
		return
	}

	var admissionResponse *admission.AdmissionResponse
	ar := admission.AdmissionReview{}
	if _, _, err := deserializer.Decode(body, nil, &ar); err != nil {
		log.Errorf("Can't decode body: %v", err)
		admissionResponse = &admission.AdmissionResponse{
			Result: &metav1.Status{
				Message: err.Error(),
			},
		}
	} else {
		if r.URL.Path == "/validate" {
			admissionResponse = whsvr.validate(&ar)
		}
	}

	admissionReview := admission.AdmissionReview{}
	admissionReview.TypeMeta = metav1.TypeMeta{Kind: "AdmissionReview", APIVersion: "admission.k8s.io/v1"}
	if admissionResponse != nil {
		admissionReview.Response = admissionResponse
		admissionReview.Response.UID = ar.Request.UID
	}

	resp, err := json.Marshal(admissionReview)
	if err != nil {
		log.Errorf("Can't encode response: %v", err)
		http.Error(w, fmt.Sprintf("could not encode response: %v", err), http.StatusInternalServerError)
	}
	if _, err := w.Write(resp); err != nil {
		log.Errorf("Can't write response: %v", err)
		http.Error(w, fmt.Sprintf("could not write response: %v", err), http.StatusInternalServerError)
	}

	log.Debugf("%+v", admissionReview)
	log.Info("Webhook Request completed.")
}

// SetWebhookCABundle updates a validating webhook configuration,
// setting the client config CABundle with the contents of certPEM.
func SetWebhookCABundle(webhookName string, clientset *kubernetes.Clientset, certPEM []byte) error {
	validatingWebhook := clientset.AdmissionregistrationV1().ValidatingWebhookConfigurations()

	configuration, err := validatingWebhook.Get(context.TODO(), webhookName, metav1.GetOptions{})
	if err != nil {
		return errors.Wrapf(err, "could not get validating webhook with name %q", webhookName)
	}

	if len(configuration.Webhooks) != 1 {
		return errors.Errorf("expected one webhook in configuration, found %d", len(configuration.Webhooks))
	}

	configuration.Webhooks[0].ClientConfig.CABundle = certPEM

	_, err = validatingWebhook.Update(context.TODO(), configuration, metav1.UpdateOptions{FieldManager: "landb_sync"})
	if err != nil {
		return errors.Wrap(err, "could not update validating webhook")
	}

	return nil
}
