// Copyright CERN.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dns

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"strings"

	"github.com/cert-manager/cert-manager/pkg/acme/webhook/apis/acme/v1alpha1"
	cmmeta "github.com/cert-manager/cert-manager/pkg/apis/meta/v1"
	log "github.com/sirupsen/logrus"
	"golang.org/x/exp/slices"
	extapi "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	"gitlab.cern.ch/kubernetes/networking/landb-sync/csrest"
	"gitlab.cern.ch/kubernetes/networking/landb-sync/pkg/version"
)

// var GroupName = os.Getenv("GROUP_NAME")

// func Main() {
// 	if GroupName == "" {
// 		panic("GROUP_NAME must be specified")
// 	}

// 	// This will register our custom DNS provider with the webhook serving
// 	// library, making it available as an API under the provided GroupName.
// 	// You can register multiple DNS provider implementations with a single
// 	// webhook, where the Name() method will be used to disambiguate between
// 	// the different implementations.
// 	cmd.RunWebhookServer(GroupName,
// 		&CernDNSProviderSolver{},
// 	)
// }

// CernDNSProviderSolver implements the provider-specific logic needed to
// 'present' an ACME challenge TXT record for your own DNS provider.
// To do so, it must implement the `github.com/jetstack/cert-manager/pkg/acme/webhook.Solver`
// interface.
type CernDNSProviderSolver struct {
	// If a Kubernetes 'clientset' is needed, you must:
	// 1. uncomment the additional `client` field in this structure below
	// 2. uncomment the "k8s.io/client-go/kubernetes" import at the top of the file
	// 3. uncomment the relevant code in the Initialize method below
	// 4. ensure your webhook's service account has the required RBAC role
	//    assigned to it for interacting with the Kubernetes APIs you need.
	client *kubernetes.Clientset
}

// cernDNSProviderConfig is a structure that is used to decode into when
// solving a DNS01 challenge.
// This information is provided by cert-manager, and may be a reference to
// additional configuration that's needed to solve the challenge for this
// particular certificate or issuer.
// This typically includes references to Secret resources containing DNS
// provider credentials, in cases where a 'multi-tenant' DNS solver is being
// created.
// If you do *not* require per-issuer or per-certificate configuration to be
// provided to your webhook, you can skip decoding altogether in favour of
// using CLI flags or similar to provide configuration.
// You should not include sensitive information here. If credentials need to
// be used by your provider here, you should reference a Kubernetes Secret
// resource and fetch these credentials using a Kubernetes clientset.
type cernDNSProviderConfig struct {
	// Change the two fields below according to the format of the configuration
	// to be decoded.
	// These fields will be set by users in the
	// `issuer.spec.acme.dns01.providers.webhook.config` field.
	TokenSecretRef cmmeta.SecretKeySelector `json:"tokenSecretRef"`
	Namespace      string
}

// Name is used as the name for this DNS solver when referencing it on the ACME
// Issuer resource.
// This should be unique **within the group name**, i.e. you can have two
// solvers configured with the same Name() **so long as they do not co-exist
// within a single webhook deployment**.
// For example, `cloudflare` may be used as the name of a solver.
func (c *CernDNSProviderSolver) Name() string {
	return "cern"
}

func (c *CernDNSProviderSolver) getClient() *csrest.APIClient {
	cfg := csrest.NewConfiguration()
	cfg.UserAgent = fmt.Sprintf("landb-sync-dns-solver/%s", version.Version())

	// new client
	client := csrest.NewAPIClient(cfg)
	return client
}

func (c *CernDNSProviderSolver) getContext(cfg *cernDNSProviderConfig) context.Context {
	secretName := cfg.TokenSecretRef.LocalObjectReference.Name
	log.Infof("Try to load secret `%s/%s` with key `%s`", secretName, cfg.Namespace, cfg.TokenSecretRef.Key)
	sec, err := c.client.CoreV1().Secrets(cfg.Namespace).Get(context.Background(), secretName, metav1.GetOptions{})
	if err != nil {
		log.Errorf("Unable to get secret `%s`: %v", secretName, err)
		return nil
	}

	secBytes, ok := sec.Data[cfg.TokenSecretRef.Key]
	if !ok {
		log.Errorf("Key %q not found in secret \"%s/%s\"", cfg.TokenSecretRef.Key, cfg.TokenSecretRef.LocalObjectReference.Name, cfg.Namespace)
		return nil
	}

	switch cfg.TokenSecretRef.Key {
	case "auth":
		auth := string(secBytes)
		log.Debugf("Retrieved auth in secret is %s", auth)

		return context.WithValue(context.Background(), csrest.ContextBasicAuth, csrest.BasicAuth{
			UserName: strings.Split(auth, ":")[0],
			Password: strings.Split(auth, ":")[1],
		})
	default:
		log.Errorf("Authentication method `%s` not configured.", cfg.TokenSecretRef.Key)
	}
	return nil
}

func (c *CernDNSProviderSolver) getRecords(cfg *cernDNSProviderConfig, dnsName string) ([]csrest.DnsRecord, error) {
	dnsRecords, resp, err := c.getClient().DNSRecordsApi.GetRecords(c.getContext(cfg), dnsName).Execute()
	if err != nil {
		return nil, fmt.Errorf("error getting DNS records for dnsName %s: %v", dnsName, err)
	}
	switch resp.StatusCode {
	case http.StatusOK:
		log.Infof("DNS records for dnsName %s: %v", dnsName, dnsRecords)
		return dnsRecords, nil
	case 401, 403:
		return nil, errors.New("failed AuthZ/AuthN against the server")
	case 500:
		return nil, errors.New("server side error")
	default:
		return nil, fmt.Errorf("statusCode %d not implemented", resp.StatusCode)
	}
}

func (c *CernDNSProviderSolver) setRecords(cfg *cernDNSProviderConfig, dnsName string, records []csrest.DnsRecord) ([]csrest.DnsRecord, error) {
	dnsRecords, err := c.getRecords(cfg, dnsName)
	if err != nil {
		return nil, fmt.Errorf("failed to set records: %v", err)
	}

	var newRecords []csrest.DnsRecord
	var updateRecords []csrest.DnsRecord
	for _, r := range records {
		// Find record  with equal Label and Type
		idx := slices.IndexFunc(dnsRecords, func(f csrest.DnsRecord) bool {
			return f.Label == r.Label && f.Type == r.Type
		})
		if idx == -1 {
			// DNS record not found, add it
			log.Debugf("New record to add %v.", r)
			newRecords = append(newRecords, r)
		} else {
			// Partial DNS record found
			if reflect.DeepEqual(r, dnsRecords[idx]) {
				log.Infof("Record %v exists, skipping...", r)
				continue
			} else {
				r.Uuid = dnsRecords[idx].Uuid
				r.Description = dnsRecords[idx].Description
				r.Version = dnsRecords[idx].Version
				log.Debugf("Adding record to update %v.", r)
				updateRecords = append(updateRecords, r)
			}
		}
	}

	// Create records if any
	for _, r := range newRecords {
		log.Infof("Creating new record %v", r)
		_, resp, err := c.getClient().DNSRecordsApi.CreateRecord(c.getContext(cfg), dnsName).DnsRecord(r).Execute()
		if err != nil {
			return nil, fmt.Errorf("execution failed for dnsName %s: %v", dnsName, err)
		}
		switch resp.StatusCode {
		case http.StatusOK:
			log.Infof("Successfully set DNS records into dnsName %s", dnsName)
			return dnsRecords, nil
		case 401, 403:
			return nil, errors.New("failed AuthZ/AuthN against the server")
		case 500:
			return nil, errors.New("server side error")
		default:
			return nil, fmt.Errorf("statusCode %d not implemented", resp.StatusCode)
		}
	}

	// Update records if any
	for _, r := range updateRecords {
		log.Infof("Updating existing record %v", r)
		_, resp, err := c.getClient().DNSRecordsApi.UpdateRecord(c.getContext(cfg), dnsName, r.Uuid).DnsRecord(r).Execute()
		if err != nil {
			return nil, fmt.Errorf("execution failed for dnsName %s: %v", dnsName, err)
		}
		switch resp.StatusCode {
		case http.StatusOK:
			log.Infof("Successfully set DNS records into dnsName %s", dnsName)
			return dnsRecords, nil
		case 401, 403:
			return nil, errors.New("failed AuthZ/AuthN against the server")
		case 500:
			return nil, errors.New("server side error")
		default:
			return nil, fmt.Errorf("statusCode %d not implemented", resp.StatusCode)
		}
	}
	return append(newRecords, updateRecords...), nil
}

func (c *CernDNSProviderSolver) delRecords(cfg *cernDNSProviderConfig, dnsName string, records []csrest.DnsRecord) ([]csrest.DnsRecord, error) {
	dnsRecords, err := c.getRecords(cfg, dnsName)
	if err != nil {
		return nil, fmt.Errorf("failed to delete records: %v", err)
	}

	var delRecords []csrest.DnsRecord
	for _, r := range records {
		// Find record  with equal Label, Key and Type
		idx := slices.IndexFunc(dnsRecords, func(f csrest.DnsRecord) bool {
			return r.Label == f.Label && r.Value == f.Value && r.Type == f.Type
		})
		if idx != -1 {
			delRecords = append(delRecords, dnsRecords[idx])
		} else {
			log.Infof("DNSName %s does not contain %s record with label=%s, value=%s.", dnsName, r.Type, r.Label, r.Value)
		}
	}

	// Delete records if any
	for _, r := range delRecords {
		log.Infof("Deleting record %+v.", r)
		resp, err := c.getClient().DNSRecordsApi.DeleteRecord(c.getContext(cfg), dnsName, r.Uuid).Version(*r.Version).Execute()
		if err != nil {
			return nil, fmt.Errorf("execution failed for dnsName %s: %v", dnsName, err)
		}
		switch resp.StatusCode {
		case http.StatusOK:
			log.Infof("Successfully deleted DNS record from dnsName %s", dnsName)
			return dnsRecords, nil
		case 401, 403:
			return nil, errors.New("failed AuthZ/AuthN against the server")
		case 500:
			return nil, errors.New("server side error")
		default:
			return nil, fmt.Errorf("statusCode %d not implemented", resp.StatusCode)
		}
	}
	return delRecords, nil
}

// Present is responsible for actually presenting the DNS record with the
// DNS provider.
// This method should tolerate being called multiple times with the same value.
// cert-manager itself will later perform a self check to ensure that the
// solver has correctly configured the DNS provider.
func (c *CernDNSProviderSolver) Present(ch *v1alpha1.ChallengeRequest) error {
	cfg, err := loadConfig(ch.Config, ch.ResourceNamespace)
	if err != nil {
		return err
	}
	log.Debugf("Present - Kubernetes received object: %+v", ch)
	log.Infof("Present for entry=%s, key=%s, dnsName=%s", strings.Split(ch.ResolvedFQDN, ".")[0], ch.Key, ch.DNSName)

	// strings.ToUpper() here so the label can be compatible with CERN landb decision to uppercase the labels
	_, err = c.setRecords(&cfg, ch.DNSName, []csrest.DnsRecord{{
		Label: strings.ToUpper(strings.Split(ch.ResolvedFQDN, ".")[0]),
		Type:  "TXT",
		Ttl:   0,
		Value: ch.Key,
		Scope: "BOTH",
	}})
	if err != nil {
		log.Errorf("Failed to present record: %v", err)
	}
	return nil
}

// CleanUp should delete the relevant TXT record from the DNS provider console.
// If multiple TXT records exist with the same record name (e.g.
// _acme-challenge.example.com) then **only** the record with the same `key`
// value provided on the ChallengeRequest should be cleaned up.
// This is in order to facilitate multiple DNS validations for the same dnsName
// concurrently.
func (c *CernDNSProviderSolver) CleanUp(ch *v1alpha1.ChallengeRequest) error {
	cfg, err := loadConfig(ch.Config, ch.ResourceNamespace)
	if err != nil {
		return err
	}
	log.Debugf("CleanUp - Kubernetes received object: %+v", ch)
	log.Infof("CleanUp for entry=%s, key=%s, dnsName=%s", strings.Split(ch.ResolvedFQDN, ".")[0], ch.Key, ch.DNSName)

	// strings.ToUpper() here so the label can be compatible with CERN landb decision to uppercase the labels
	_, err = c.delRecords(&cfg, ch.DNSName, []csrest.DnsRecord{{
		Label: strings.ToUpper(strings.Split(ch.ResolvedFQDN, ".")[0]),
		Type:  "TXT",
		Ttl:   0,
		Value: ch.Key,
		Scope: "BOTH",
	}})
	if err != nil {
		log.Errorf("Failed to clean record: %v", err)
	}
	return nil
}

// Initialize will be called when the webhook first starts.
// This method can be used to instantiate the webhook, i.e. initialising
// connections or warming up caches.
// Typically, the kubeClientConfig parameter is used to build a Kubernetes
// client that can be used to fetch resources from the Kubernetes API, e.g.
// Secret resources containing credentials used to authenticate with DNS
// provider accounts.
// The stopCh can be used to handle early termination of the webhook, in cases
// where a SIGTERM or similar signal is sent to the webhook process.
func (c *CernDNSProviderSolver) Initialize(kubeClientConfig *rest.Config, stopCh <-chan struct{}) error {
	cl, err := kubernetes.NewForConfig(kubeClientConfig)
	if err != nil {
		return err
	}

	c.client = cl
	return nil
}

// loadConfig is a small helper function that decodes JSON configuration into
// the typed config struct.
func loadConfig(cfgJSON *extapi.JSON, namespace string) (cernDNSProviderConfig, error) {
	cfg := cernDNSProviderConfig{}
	cfg.Namespace = namespace
	// handle the 'base case' where no configuration has been provided
	if cfgJSON == nil {
		return cfg, nil
	}
	if err := json.Unmarshal(cfgJSON.Raw, &cfg); err != nil {
		return cfg, fmt.Errorf("error decoding solver config: %v", err)
	}

	return cfg, nil
}
