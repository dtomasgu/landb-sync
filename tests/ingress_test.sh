# Copyright CERN.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#!/bin/bash

RED='\033[0;31m'
YELLOW='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m'

###############################################################################
###############################################################################
###############################################################################
# Functions

get_openstack_landb_alias_from_server () {
  echo $(openstack server show -f json $1 | jq .properties  | sed "s/'//g" | jq  -r '."landb-alias" ')
}

clean_openstack_landb_alias () {
  kubectl delete namespace testing
  sleep $SLEEP_TIME

  echo "Cleaning aliases on all nodes"
  # clean landb-alias property
  for node in $@
  do
    echo $node
    openstack server unset --property landb-alias $node
  done
}

get_random_name () {
  echo $(head -c 1000 /dev/urandom | tr -dc 'a-z0-9' | fold -w 16 | head -n 1)
}

###############################################################################
###############################################################################
###############################################################################
# Set Up

kubectl create namespace testing

declare -a NODES
NODES=$(kubectl get nodes -l role=ingress | grep node | sort -k1 | cut -d' ' -f1)

# Get number of nodes
TOTAL_NODES=0
for node in ${NODES[@]}
do
  ((TOTAL_NODES=TOTAL_NODES+1))
done
if [ "$TOTAL_NODES" -le "0" ]; then
  echo -e  "${YELLOW}There are no ingress nodes${NC}"
  clean_openstack_landb_alias $NODES
fi

# Get Sleep time per number of ingress nodes
((SLEEP_TIME=30*TOTAL_NODES))

# Ingress name randomizer
RAND_NAME="x$(get_random_name)"

# Write files
cat << EOF > original-alias.yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: original-alias
  namespace: testing
  annotations:
    kubernetes.io/ingress.class: traefik
    traefik.frontend.entryPoints: "http"
    traefik.ingress.kubernetes.io/router.entrypoints: http
spec:
  rules:
  - host: ${RAND_NAME}.cern.ch
    http:
      paths:
      - path: /
        backend:
          service:
            name: ingress-traefik
            port:
              number: 8080
        pathType: ImplementationSpecific
EOF

cat << EOF > alternate-alias.yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: alternate-alias
  namespace: testing
  annotations:
    kubernetes.io/ingress.class: traefik
    traefik.frontend.entryPoints: "http"
    traefik.ingress.kubernetes.io/router.entrypoints: http
spec:
  rules:
  - host: ${RAND_NAME}.cern.ch
    http:
      paths:
      - path: /test
        backend:
          service:
            name: ingress-traefik
            port:
              number: 8080
        pathType: ImplementationSpecific
EOF

###############################################################################
###############################################################################
###############################################################################
# Tests

echo "............................................................."
echo -e "${GREEN}             STARTING END-TO-END TEST SUITE${NC}"
echo "............................................................."

# create one ingress and validate that it is correctly assigned to the different
# role=ingress nodes
###############################################################################
echo -n "create one ingress..........................................."
kubectl apply -f ./original-alias.yaml >> /dev/null
sleep $SLEEP_TIME
NODES_NUMBER=0
for node in ${NODES[@]}
do
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [[ $OS_NODE == "${RAND_NAME}--load-${NODES_NUMBER}-" ]]; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

# Update ingress and validate that it is correctly assigned to the different
# role=ingress nodes
###############################################################################
echo -n "update one ingress..........................................."
kubectl -n testing patch ingress original-alias --type=json \
  -p='[{"op": "replace", "path": "/spec/rules/0/host", "value":"updated-'${RAND_NAME}'.cern.ch"}]' &> /dev/null
sleep $SLEEP_TIME
NODES_NUMBER=0
for node in ${NODES[@]}
do
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [[ $OS_NODE == "updated-${RAND_NAME}--load-${NODES_NUMBER}-" ]]; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

# Delete ingress and validate that it is correctly assigned to the different
# role=ingress nodes
###############################################################################
echo -n "delete one ingress..........................................."
kubectl delete -f ./original-alias.yaml >> /dev/null
sleep $SLEEP_TIME
NODES_NUMBER=0
for node in ${NODES[@]}
do
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [ $OS_NODE == "null" ]; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

# create two ingress and validate that it is correctly assigned to the different
# role=ingress nodes
###############################################################################
echo -n "create two ingresses with same hostname......................"
kubectl apply -f ./original-alias.yaml >> /dev/null
kubectl apply -f ./alternate-alias.yaml >> /dev/null
sleep $SLEEP_TIME
NODES_NUMBER=0
for node in ${NODES[@]}
do
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [[ $OS_NODE == "${RAND_NAME}--load-${NODES_NUMBER}-" ]] ; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

# Update ingress and validate that it is correctly assigned to the different
# role=ingress nodes
###############################################################################
echo -n "update one ingress from the duplicated hostname.............."
kubectl -n testing patch ingress alternate-alias --type=json \
  -p='[{"op": "replace", "path": "/spec/rules/0/host", "value":"updated-'${RAND_NAME}'.cern.ch"}]' &> /dev/null
sleep $SLEEP_TIME
NODES_NUMBER=0
for node in ${NODES[@]}
do
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [[ $OS_NODE == "updated-${RAND_NAME}--load-${NODES_NUMBER}-,${RAND_NAME}--load-${NODES_NUMBER}-" ]]; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

# Delete one ingress from the repeated hostnames and validate that the hostname
# is still existent
###############################################################################
echo -n "delete one ingress with same hostname........................"
kubectl -n testing patch ingress alternate-alias --type=json -p='[{"op": "replace", "path": "/spec/rules/0/host", "value":"${RAND_NAME}.cern.ch"}]' &> /dev/null
sleep $SLEEP_TIME
kubectl delete -f ./alternate-alias.yaml >> /dev/null
sleep $SLEEP_TIME
NODES_NUMBER=0
for node in ${NODES[@]}
do
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [[ $OS_NODE == "${RAND_NAME}--load-${NODES_NUMBER}-" ]]; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

# Delete all ingresses from the repeated hostnames
###############################################################################
echo -n "delete all ingresses........................................."
kubectl delete -f ./original-alias.yaml >> /dev/null
sleep $SLEEP_TIME
NODES_NUMBER=0
for node in ${NODES[@]}
do
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [ $OS_NODE == "null" ]; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

# create new ingress definition with existent user defined landb-alias
########################################################s######################
echo -n "create one ingress with one user defined alias..............."
NODES_NUMBER=0
# add custom alias to nodes
for node in ${NODES[@]}
do
  ((NODES_NUMBER=NODES_NUMBER+1))
  openstack server set --property landb-alias=fake-user-hostname-${RAND_NAME}--load-${NODES_NUMBER}- $node
done
sleep $SLEEP_TIME
# Test kubernetes ingress case
NODES_NUMBER=0
kubectl apply -f ./original-alias.yaml >> /dev/null
sleep $SLEEP_TIME
for node in ${NODES[@]}
do
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [[ $OS_NODE == "fake-user-hostname-${RAND_NAME}--load-${NODES_NUMBER}-,${RAND_NAME}--load-${NODES_NUMBER}-" ]] ; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

# Update ingress and validate that it is correctly assigned to the different
# role=ingress nodes
###############################################################################
echo -n "update one ingress with one user defined alias..............."
kubectl -n testing patch ingress original-alias --type=json \
  -p='[{"op": "replace", "path": "/spec/rules/0/host", "value":"updated-'${RAND_NAME}'.cern.ch"}]' &> /dev/null
sleep $SLEEP_TIME
NODES_NUMBER=0
for node in ${NODES[@]}
do
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [[ $OS_NODE == "fake-user-hostname-${RAND_NAME}--load-${NODES_NUMBER}-,updated-${RAND_NAME}--load-${NODES_NUMBER}-" ]]; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

# Delete all kubernetes defined ingresses making the user remaining indress the
# only one available
###############################################################################
echo -n "delete all defined ingress with one user defined alias......."
kubectl delete -f ./original-alias.yaml >> /dev/null
sleep $SLEEP_TIME
NODES_NUMBER=0
for node in ${NODES[@]}
do
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [[ $OS_NODE == "fake-user-hostname-${RAND_NAME}--load-${NODES_NUMBER}-" ]]; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

# create new ingress definition with existent user defined landb-alias
########################################################s######################
echo -n "create kubernetes ingress with user defined alias reverse...."
NODES_NUMBER=0
REVERSE_NODES_NUMBER=0
# add custom alias to nodes
for node in ${NODES[@]}
do
  ((REVERSE_NODES_NUMBER=(($TOTAL_NODES-${NODES_NUMBER}))))
  ((NODES_NUMBER=NODES_NUMBER+1))
  openstack server set --property landb-alias=fake-user-hostname-${RAND_NAME}--load-${NODES_NUMBER}-,fake-user-reverse-hostname-${RAND_NAME}--load-${REVERSE_NODES_NUMBER}- $node
done
sleep $SLEEP_TIME
# Test kubernetes ingress case
NODES_NUMBER=0
kubectl apply -f ./original-alias.yaml >> /dev/null
sleep $SLEEP_TIME
for node in ${NODES[@]}
do
  ((REVERSE_NODES_NUMBER=(($TOTAL_NODES-${NODES_NUMBER}))))
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [[ $OS_NODE == "fake-user-hostname-${RAND_NAME}--load-${NODES_NUMBER}-,fake-user-reverse-hostname-${RAND_NAME}--load-${REVERSE_NODES_NUMBER}-,${RAND_NAME}--load-${NODES_NUMBER}-" ]] ; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

# Update ingress and validate that it is correctly assigned to the different
# role=ingress nodes
###############################################################################
echo -n "update ingress with user defined alias reverse..............."
kubectl -n testing patch ingress original-alias --type=json \
  -p='[{"op": "replace", "path": "/spec/rules/0/host", "value":"updated-'${RAND_NAME}'.cern.ch"}]' &> /dev/null
sleep $SLEEP_TIME
NODES_NUMBER=0
REVERSE_NODES_NUMBER=0
for node in ${NODES[@]}
do
  ((REVERSE_NODES_NUMBER=(($TOTAL_NODES-${NODES_NUMBER}))))
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [[ $OS_NODE == "fake-user-hostname-${RAND_NAME}--load-${NODES_NUMBER}-,fake-user-reverse-hostname-${RAND_NAME}--load-${REVERSE_NODES_NUMBER}-,updated-${RAND_NAME}--load-${NODES_NUMBER}-" ]]; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

# Delete all kubernetes defined ingresses making the user remaining indress the
# only one available
###############################################################################
echo -n "delete kubernetes ingress with user defined alias reverse...."
kubectl delete -f ./original-alias.yaml >> /dev/null
sleep $SLEEP_TIME
NODES_NUMBER=0
REVERSE_NODES_NUMBER=0
for node in ${NODES[@]}
do
  ((REVERSE_NODES_NUMBER=(($TOTAL_NODES-${NODES_NUMBER}))))
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE=$( get_openstack_landb_alias_from_server ${node} )
  if [[ $OS_NODE == "fake-user-hostname-${RAND_NAME}--load-${NODES_NUMBER}-,fake-user-reverse-hostname-${RAND_NAME}--load-${REVERSE_NODES_NUMBER}-" ]]; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    clean_openstack_landb_alias $NODES
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"

echo "............................................................."
echo -e "${GREEN}                    ALL TESTS PASSED${NC}"
echo "............................................................."

clean_openstack_landb_alias $NODES

exit 0
