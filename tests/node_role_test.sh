# Copyright CERN.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#!/bin/bash

RED='\033[0;31m'
YELLOW='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m'

###############################################################################
###############################################################################
###############################################################################
# Functions

###############################################################################
###############################################################################
###############################################################################
# Set Up
INTENDED_INGRESS_NODES=2

declare -a INGRESS_NODES
INGRESS_NODES=$(kubectl get nodes -l role=ingress | grep node | sort -k1 | cut -d' ' -f1)
NODES=$(kubectl get nodes | grep -v master | grep -v NAME | sort -k1 | cut -d' ' -f1)

# Get number of nodes
TOTAL_INGRESS_NODES=0
for node in ${INGRESS_NODES[@]}
do
  ((TOTAL_INGRESS_NODES=TOTAL_INGRESS_NODES+1))
done
TOTAL_NODES=0
for node in ${NODES[@]}
do
  ((TOTAL_NODES=TOTAL_NODES+1))
done

echo "............................................................."
echo -e "${GREEN}          STARTING NODE ROLE VALIDATION SUITE${NC}"
echo "............................................................."

# Validate if the number of ingress nodes is equal to the intended
###############################################################################
echo -n "check the number of ingress nodes match target..............."
if [ "$TOTAL_INGRESS_NODES" -le 0 ]; then
  echo -e  "${RED}FAIL${NC}"
  exit -1
fi
if [ "$TOTAL_INGRESS_NODES" -le "$INTENDED_INGRESS_NODES" ]; then
  if [ "$TOTAL_INGRESS_NODES" -eq "$TOTAL_NODES" ]; then
    echo -e  "${GREEN}OK${NC}"
  else
    echo -e  "${YELLOW}OK${NC}"
  fi
else
  echo -e "${RED}FAIL${NC}"
  exit -1
fi

echo "............................................................."
echo -e "${GREEN}                    ALL TESTS PASSED${NC}"
echo "............................................................."

exit 0
