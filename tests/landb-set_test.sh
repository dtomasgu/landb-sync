# Copyright CERN.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#!/bin/bash

RED='\033[0;31m'
YELLOW='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m'

###############################################################################
###############################################################################
###############################################################################
# Functions

get_openstack_landb_set_from_server () {
  echo $(openstack server show -f json $1 | jq .properties  | sed "s/'//g" | jq -r 'split(", ") | map(split("=")) | map({"key":(.[0]), "value":(.[1])}) | from_entries | ."landb-set"')
}

###############################################################################
###############################################################################
###############################################################################
# Set Up
LANDB_SET_NAME="OPENSTACK TEST"

declare -a NODES
NODES=$(kubectl get nodes -l role=ingress | grep node | sort -k1 | cut -d' ' -f1)

# Get number of nodes
TOTAL_NODES=0
for node in ${NODES[@]}
do
  ((TOTAL_NODES=TOTAL_NODES+1))
done
if [ "$TOTAL_NODES" -le "0" ]; then
  echo -e  "${YELLOW}There are no ingress nodes${NC}"
  clean_openstack_landb_set $NODES
fi

###############################################################################
###############################################################################
###############################################################################
# Tests

echo "............................................................."
echo -e "${GREEN}          STARTING LANDB-SET VALIDATION SUITE${NC}"
echo "............................................................."

# check if all ingress nodes have the landb-set property
###############################################################################
echo -n "check if all ingress nodes have the landb-set property......."
NODES_NUMBER=0
for node in ${NODES[@]}
do
  ((NODES_NUMBER=NODES_NUMBER+1))
  OS_NODE="$( get_openstack_landb_set_from_server ${node} )"
  if [[ "${OS_NODE}" == "${LANDB_SET_NAME}" ]]; then
    continue
  else
    echo -e "${RED}FAIL${NC}"
    exit -1
  fi
done
echo -e  "${GREEN}OK${NC}"


echo "............................................................."
echo -e "${GREEN}                    ALL TESTS PASSED${NC}"
echo "............................................................."

exit 0
